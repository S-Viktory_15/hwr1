import React from 'react'
import ReactDOM from 'react-dom'

function Zodiac(){
    return (
        <div>
            <h2>Знаки зодіаку</h2>
            
            <tbody>
                <tr>
                    <td>Овен</td>
                    <td>21.03 - 20.04</td>
                </tr>
                <tr>
                    <td>Тілець</td>
                    <td>21.04 - 20.05</td>
                </tr>
                <tr>
                    <td>Близнюки</td>
                    <td>21.05 - 20.06</td>
                </tr>
                <tr>
                    <td>Рак</td>
                    <td>21.06 - 22.07</td>
                </tr>
                <tr>
                    <td>Лев</td>
                    <td>23.07 - 22.08</td>
                </tr>
                <tr>
                    <td>Діва</td>
                    <td>23.08 - 22.09</td>
                </tr>
                <tr>
                    <td>Ваги</td>
                    <td>23.09 - 22.10</td>
                </tr>
                <tr>
                    <td>Скорпіон</td>
                    <td>23.10 - 22.11</td>
                </tr>
                <tr>
                    <td>Стрілець</td>
                    <td>23.11 - 21.12</td>
                </tr>
                <tr>
                    <td>Козеріг</td>
                    <td>22.12 - 19.01</td>
                </tr>
                <tr>
                    <td>Водолій</td>
                    <td>20.01 - 19.02</td>
                </tr>
                <tr>
                    <td>Риби</td>
                    <td>20.02 - 20.03</td>
                </tr>

            </tbody>
        </div>
    )
}
ReactDOM.render(<Zodiac></Zodiac>, document.querySelector(".zodiac"));